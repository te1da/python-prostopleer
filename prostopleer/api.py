# -*- coding: utf-8 -*-

import requests
import auth

class API(object):
    """Basic class for interracting with server"""
    def __init__(self, client_id=None, client_secret=None):
        """Constructor for API object. Takes OAuth2 Client Credentials."""
        self.client_id = client_id
        self.client_secret = client_secret
        self.access_token = None
    def call(self, method, **args):
        """Calls pleer.com API method. Returns server response converted to python dict. If error exeption is thrown"""
        if self.access_token != None:
            try:
                rj = self._call(method, **args)
            except PleerError:
                pass
            else:
                return rj
        self.access_token = auth.get_access_token(self.client_id, self.client_secret)
        rj = self._call(method, **args)
        return rj


    def _call(self, method, **args):
        session = requests.Session()
        args['method'] = method
        args['access_token'] = self.access_token
        try:
            response = session.post('http://api.pleer.com/index.php', args)
            rj = response.json()
        except BaseException as e:
            raise PleerError(e) #TODO
        if u'success' in rj and rj[u'success'] == True:
            return rj
        else:
            raise PleerError(json_response=rj) #TODO
        
class PleerError(Exception):
    """Exception thrown if server response is malformed or server indicates an error."""
    def __init__(self, *args, **kwargs):
        self.json_response = None
        """Non-parsed server response"""
        if 'json_response' in kwargs:
            self.json_response = kwargs['json_response']
        super(PleerError, self).__init__(*args)
