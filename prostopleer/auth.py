# -*- coding: utf-8 -*-

import requests


def get_access_token(client_id=None, client_secret=None):
    session = requests.Session()
    if client_id != None and client_secret != None:
        oauth_data = {'grant_type':'client_credentials',}
        response = session.post('http://api.pleer.com/token.php', oauth_data, auth=(client_id, client_secret))
        response_json = response.json()
        if u'access_token' in response_json:
            return response_json[u'access_token']
